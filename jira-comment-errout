#!/bin/sh
# shellcheck disable=SC2317
# scrippies/jira-comment-errout

set -eu

export LC_ALL=C

this="$(realpath "$0")"
readonly this="${this}"
here="$(dirname "${this}")"
readonly here="${here}"
whatami="$(basename "${this}")"
readonly whatami="${whatami}"
tmpdir="$(
    mktemp -dt "${whatami}.XXXXXX" \
        | xargs -I{} sh -c "chmod 1777 {} && echo {}"
)"
readonly tmpdir="${tmpdir}"
export TMPDIR="${tmpdir}"

cleanup() {
    status="$?"
    find "${tmpdir}" -type f -exec shred {} +
    rm -rf "${tmpdir}" || true
    return "${status}"
}
trap cleanup EXIT

log() { echo "${whatami}[$$]:" "$@" >&2; }
error() { log "ERROR:" "$@"; }
warning() { log "WARNING:" "$@"; }
info() { log "INFO:" "$@"; }

die() {
    error "$@"
    usage >&2
    exit 1
}

usage() {
    cat <<EOF

Usage: $0 ISSUE_KEY CMD...
Execute CMD and add its stderr/stdout to ISSUE_KEY as a comment.

Examples:

    \$ $0 RAPID-31723 uname -a

    \$ $0 RAPID-31723 sh -c "( set -euvx ; date -uIseconds ; )"

EOF
}

################################################################################
################################################################################
################################################################################

if ! command -v script >/dev/null 2>&1; then
    die missing command: script
fi

if ! command -v jira >/dev/null 2>&1; then
    die missing command: jira
fi

if ! [ "$#" -ge 2 ]; then
    die bad args
fi

ISSUE_KEY="$1"
shift
if ! jira issue view --plain "${ISSUE_KEY}" >/dev/null 2>&1; then
    die FAILURE: jira issue view --plain "${ISSUE_KEY}"
fi

log_out="$(mktemp -t log_out.XXXXXX)"
if script --return --log-out="${log_out}" --command="$*"; then
    true
else
    error '$?': "$?"
    die FAILURE: "$@"
fi

sed \
    -e 's/\x1b\[[0-9;?]*[JKmsu]//g' \
    -e 's/[^\x00-\x7f]//g' \
    -e '1i```' \
    -e '$a```' \
    <"${log_out}" \
    | jira issue comment add "${ISSUE_KEY}" --no-input --template -

exit "$?"
