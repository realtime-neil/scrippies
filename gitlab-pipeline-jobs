#!/bin/sh
# shellcheck disable=SC2317
# scrippies/gitlab-pipeline-jobs

set -eu

export LC_ALL=C

this="$(realpath "$0")"
readonly this="${this}"
here="$(dirname "${this}")"
readonly here="${here}"
whatami="$(basename "${this}")"
readonly whatami="${whatami}"
tmpdir="$(
    mktemp -dt "${whatami}.XXXXXX" \
        | xargs -I{} sh -c "chmod 1777 {} && echo {}"
)"
readonly tmpdir="${tmpdir}"
export TMPDIR="${tmpdir}"

cleanup() {
    status="$?"
    find "${tmpdir}" -type f -exec shred {} +
    rm -rf "${tmpdir}" || true
    return "${status}"
}
trap cleanup EXIT

log() { echo "${whatami}"'['"$$"']:' "$@" >&2; }
error() { log "ERROR:" "$@"; }
warning() { log "WARNING:" "$@"; }
info() { log "INFO:" "$@"; }

die() {
    error "$@"
    usage >&2
    exit 1
}

usage() {
    cat <<EOF

Usage: $0 [OPTION]... CI_PIPELINE_URL
For the given pipeline, download the list of successful jobs.

Options:

    -h    print usage and exit

Examples:

    \$ $0 -h

    \$ $0 https://gitlab.com/realtime-robotics/build-megadeb/-/pipelines/687803383

    \$ $0 https://gitlab.com/realtime-robotics/sqa-test-monorepo/-/pipelines/687822618

Caveats:

    * Job quantity is limited to 100 because pagination is not implemented.

References:

    * https://docs.gitlab.com/ee/api/jobs.html#list-pipeline-jobs

    * https://docs.gitlab.com/ee/api/rest/index.html#offset-based-pagination

EOF
}

urlencode() {
    jq -sRr @uri
}

urldecode() {
    # https://stackoverflow.com/questions/28309728/decode-url-in-bash/28309957#28309957
    sed -E -e 's/[+]/ /g' -e 's/[%]([[:xdigit:]]{2})/\\\\x\1/g' \
        | xargs printf '%b\n'
}

# convert the given argument(s) to a POSIX-safe basename
posix_namify() {
    # https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap03.html#tag_03_282
    # "Portable Filename Character Set"
    printf '%s' "$*" | sed -E 's/[^[:alnum:].-]+/_/g;s/^_//;s/_$//'
}

################################################################################
################################################################################
################################################################################

while getopts ":h" opt; do
    case "${opt}" in
        h)
            usage
            exit 0
            ;;
        :) die "missing argument: -${OPTARG}" ;;
        \?) die "bad option: -${OPTARG}" ;;
    esac
done
shift "$((OPTIND - 1))"

################################################################################

# vet the given CI_PIPELINE_URL
if ! [ 1 -eq "$#" ]; then
    die "bad args"
fi
if ! CI_PIPELINE_URL="$(
    echo "$1" \
        | grep -Ex 'https://gitlab[.]com/([^/]+(/[^/]+)*)/[-]/pipelines/[[:digit:]]+'
)"; then
    die "bad CI_PIPELINE_URL:" "$1"
fi
info CI_PIPELINE_URL: "${CI_PIPELINE_URL}"

################################################################################

# Parse the CI_PIPELINE_URL to get the CI_PROJECT_ID; this is an url-encoded string
# of path components between 'gitlab.com/' and '/-/pipelines/[0-9]+'.
#
# https://docs.gitlab.com/ee/api/index.html#namespaced-path-encoding
CI_PROJECT_ID="$(
    printf '%s' "${CI_PIPELINE_URL}" \
        | sed -E 's|https://gitlab[.]com/([^/]+(/[^/]+)*)/[-]/pipelines/[[:digit:]]+|\1|' \
        | urlencode
)"
info CI_PROJECT_ID: "${CI_PROJECT_ID}"

# Parse the CI_PIPELINE_URL to get the CI_PIPELINE_ID; this is a natural number in
# decimal base.
CI_PIPELINE_ID="$(printf '%s' "${CI_PIPELINE_URL}" | grep -Eo '[[:digit:]]+$')"
info CI_PIPELINE_ID: "${CI_PIPELINE_ID}"

################################################################################

# Allow the caller to override this.
GITLAB_API_ENDPOINT="${GITLAB_API_ENDPOINT:-${CI_API_V4_URL:-https://gitlab.com/api/v4}}"
info GITLAB_API_ENDPOINT: "${GITLAB_API_ENDPOINT}"

################################################################################

# Discover token and generate curl header file.
curl_header_file="$(mktemp -ut curl_header_file.XXXXXX)"
if [ -n "${GITLAB_API_PRIVATE_TOKEN-}" ]; then
    warning GITLAB_API_PRIVATE_TOKEN is deprecated by GITLAB_TOKEN
    info using PRIVATE-TOKEN: '$'GITLAB_API_PRIVATE_TOKEN
    echo "PRIVATE-TOKEN:" "${GITLAB_API_PRIVATE_TOKEN}"
elif [ -n "${GITLAB_TOKEN-}" ]; then
    info using PRIVATE-TOKEN: '$'GITLAB_TOKEN
    echo "PRIVATE-TOKEN:" "${GITLAB_TOKEN}"
elif [ -n "${CI_JOB_TOKEN-}" ]; then
    info using JOB-TOKEN: '$'CI_JOB_TOKEN
    echo "JOB-TOKEN:" "${CI_JOB_TOKEN}"
else
    die "need one, missing all:" "GITLAB_TOKEN" "CI_JOB_TOKEN"
fi >"${curl_header_file}"
readonly curl_header_file="${curl_header_file}"

################################################################################

# This is the API endpoint that we can hit with curl.
#
# https://docs.gitlab.com/ee/api/jobs.html#list-pipeline-jobs
#
# > By default, this request returns 20 results at a time because the API
# > results are paginated.
#
# https://docs.gitlab.com/ee/api/rest/index.html#offset-based-pagination
#
# > Offset-based pagination
#
# > Sometimes, the returned result spans many pages. When listing resources,
# > you can pass the following parameters:
#
# > Parameter Description
#
# > 'page': Page number (default: 1).
#
# > 'per_page': Number of items to list per page (default: 20, max: 100).
#
GITLAB_PIPELINE_JOBS_URL="$(
    printf \
        '%s/projects/%s/pipelines/%u/jobs' \
        "${GITLAB_API_ENDPOINT}" \
        "${CI_PROJECT_ID}" \
        "${CI_PIPELINE_ID}"
)"
info GITLAB_PIPELINE_JOBS_URL: "${GITLAB_PIPELINE_JOBS_URL}"

# This is the file (basename) that will store the value returned from the API
# request.
GITLAB_PIPELINE_JOBS_OUT="$(
    posix_namify "$(printf '%s.json' "${GITLAB_PIPELINE_JOBS_URL}" | urldecode)"
)"
info GITLAB_PIPELINE_JOBS_OUT: "${GITLAB_PIPELINE_JOBS_OUT}"

################################################################################

info "downloading..."
curl \
    --fail \
    --retry 3 \
    --show-error \
    --location \
    --request GET \
    --data "per_page=100" \
    --data "scope=success" \
    --header @"${curl_header_file}" \
    --output "${GITLAB_PIPELINE_JOBS_OUT}" \
    "${GITLAB_PIPELINE_JOBS_URL}"

if command -v jq >/dev/null 2>&1; then
    info "reformatting..."
    jq . "${GITLAB_PIPELINE_JOBS_OUT}" >"${GITLAB_PIPELINE_JOBS_OUT}".pretty
    mv "${GITLAB_PIPELINE_JOBS_OUT}".pretty "${GITLAB_PIPELINE_JOBS_OUT}"
fi

exit "$?"
