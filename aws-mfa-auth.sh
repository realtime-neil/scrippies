# scrippies/aws-mfa-auth.sh

# shellcheck shell=sh

aws_sts_get_session_token() {
    case "$#" in
        1)
            token_code="$1"
            if [ -z "${AWS_USERNAME:-}" ]; then
                echo 'ERROR: undefined/empty: AWS_USERNAME' >&2
                return 1
            fi
            serial_number="arn:aws:iam::713716739604:mfa/${AWS_USERNAME}"
            ;;
        2)
            token_code="$1"
            serial_number="arn:aws:iam::713716739604:mfa/$2"
            ;;
        *)
            echo 'ERROR: bad args' >&2
            cat >&2 <<'EOF'

Usage: aws_sts_get_session_token MFA_TOKEN_CODE [AWS_USERNAME]

Acquire and export session token using MFA device.

EOF
            return 1
            ;;
    esac

    if [ "good" = "${AWS_ACCESS_KEY_ID:+good}" ]; then
        true
    elif aws configure get aws_access_key_id >/dev/null 2>&1; then
        true
    else
        echo "ERROR:" "undefined/empty:" "aws_access_key_id"
    fi

    if [ "good" = "${AWS_SECRET_ACCESS_KEY:+good}" ]; then
        true
    elif aws configure get aws_secret_access_key >/dev/null 2>&1; then
        true
    else
        echo "ERROR:" "undefined/empty:" "aws_secret_access_key"
    fi

    if ! AWS_STS_GET_SESSION_TOKEN="$(
        unset AWS_ACCESS_KEY_ID
        unset AWS_EXPIRATION
        unset AWS_SECRET_ACCESS_KEY
        unset AWS_SESSION_TOKEN
        aws sts get-session-token \
            --output text \
            --serial-number "${serial_number}" \
            --token-code "${token_code}"
    )"; then
        echo "ERROR:" "FAILURE:" \
            "aws sts get-session-token" \
            "--output" "text" \
            "--serial-number" "${serial_number}" \
            "--token-code" "${token_code}" \
            >&2
        return 1
    fi

    AWS_ACCESS_KEY_ID="$(echo "${AWS_STS_GET_SESSION_TOKEN}" | awk '{print $2}')"
    AWS_EXPIRATION="$(echo "${AWS_STS_GET_SESSION_TOKEN}" | awk '{print $3}')"
    AWS_SECRET_ACCESS_KEY="$(echo "${AWS_STS_GET_SESSION_TOKEN}" | awk '{print $4}')"
    AWS_SESSION_TOKEN="$(echo "${AWS_STS_GET_SESSION_TOKEN}" | awk '{print $5}')"

    export AWS_STS_GET_SESSION_TOKEN
    export AWS_ACCESS_KEY_ID
    export AWS_EXPIRATION
    export AWS_SECRET_ACCESS_KEY
    export AWS_SESSION_TOKEN

    echo "AWS_ACCESS_KEY_ID:" "${AWS_ACCESS_KEY_ID}"
    echo "AWS_EXPIRATION:" "${AWS_EXPIRATION}"
}

cat <<'EOF'

Defined shell function: aws_sts_get_session_token

Usage: aws_sts_get_session_token MFA_TOKEN_CODE [AWS_USERNAME]

Acquire and export session token using MFA device.

EOF
