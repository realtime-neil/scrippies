#!/bin/sh
# shellcheck disable=SC2317
# scrippies/push-all

set -eu

export LC_ALL=C

this="$(realpath "$0")"
readonly this="${this}"
here="$(dirname "${this}")"
readonly here="${here}"
whatami="$(basename "${this}")"
readonly whatami="${whatami}"
tmpdir="$(
    mktemp -dt "${whatami}.XXXXXX" | while read -r path; do
        chmod 1777 "${path}"
        echo "${path}"
    done
)"
readonly tmpdir="${tmpdir}"
export TMPDIR="${tmpdir}"

cleanup() {
    status="$?"
    rm -rf "${tmpdir}" || true
    return "${status}"
}
trap cleanup EXIT

log() { echo "${whatami}[$$]:" "$@" >&2; }
error() { log "ERROR:" "$@"; }
warning() { log "WARNING:" "$@"; }
info() { log "INFO:" "$@"; }

die() {
    error "$@"
    usage >&2
    exit 1
}

usage() {
    cat <<EOF

Usage: $0 [OPTION]... [-- [GIT_PUSH_ARG]...]
Push HEAD to every branch on every remote.

Options:

    -h            print usage and exit
    -C WORKDIR    run as if started in workdir (default: \$PWD)

Examples:

    \$ $0 -h

    \$ $0

    \$ $0 -C ${PWD}

    \$ $0 -C ${PWD} -- --dry-run --verbose

    \$ $0 -C ${PWD} -- --verbose 1.2.9

    \$ $0 -C ${PWD} -- --verbose --tags

EOF
}

################################################################################
################################################################################
################################################################################

while getopts ":hC:" opt; do
    case "${opt}" in
        h)
            usage
            exit 0
            ;;
        C) readonly workdir="${OPTARG}" ;;
        :) die "missing argument: -${OPTARG}" ;;
        \?) die "bad option: -${OPTARG}" ;;
    esac
done
shift "$((OPTIND - 1))"

################################################################################

if [ -n "${workdir-}" ]; then
    info "workdir: ${workdir}"
else
    readonly workdir="${PWD}"
    warning "defaulting workdir: ${workdir}"
fi

################################################################################

remote_list="$(mktemp -t remote_list.XXXXXX)"
readonly remote_list="${remote_list}"
git -C "${workdir}" remote >"${remote_list}"
sort -uo "${remote_list}" "${remote_list}"

################################################################################

branch_list="$(mktemp -t branch_list.XXXXXX)"
readonly branch_list="${branch_list}"
#
# $ git branch --remotes --format=%(refname)
# refs/remotes/gitlab/master
# refs/remotes/gitlab/unstable
#
# $ git branch --remotes --format=%(refname:lstrip=3)
# master
# unstable
git -C "${workdir}" branch --remotes --format='%(refname:lstrip=3)' >"${branch_list}"
sort -uo "${branch_list}" "${branch_list}"
sed -i '/^HEAD$/d' "${branch_list}"

################################################################################

rc=0
while read -r remote; do
    info "remote: ${remote}"
    if ! sed 's/^/HEAD:/g' "${branch_list}" \
        | xargs git -C "${workdir}" push "${remote}" "$@"; then
        rc=1
    fi
done <"${remote_list}"

################################################################################

exit "${rc}"
