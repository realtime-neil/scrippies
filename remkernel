#!/bin/sh
# shellcheck disable=SC2317
# scrippies/remkernel

set -eu

export LC_ALL=C

this="$(realpath -e "$0")"
readonly this="${this}"
here="$(dirname "${this}")"
readonly here="${here}"
whatami="$(basename "${this}")"
readonly whatami="${whatami}"
tmpdir="$(
    mktemp -dt "${whatami}.XXXXXX" | while read -r path; do
        chmod 1777 "${path}"
        echo "${path}"
    done
)"
readonly tmpdir="${tmpdir}"
export TMPDIR="${tmpdir}"

log() { echo "${whatami}[$$]:" "$@" >&2; }
error() { log "ERROR:" "$@"; }
warning() { log "WARNING:" "$@"; }
info() { log "INFO:" "$@"; }

die() {
    error "$@"
    usage >&2
    exit 1
}

cleanup() {
    status="$?"
    rm -rf "${tmpdir}" || true
    return "${status}"
}
trap cleanup EXIT

usage() {
    cat <<EOF

Usage: $0 [OPTION]...
Remove unused kernels; keep currently-running and latest.

Options:

    -h    print usage and exit
    -n    noop; stop before actually doing anything

Examples:

    \$ $0 -h

    \$ $0

    \$ $0 -n

EOF
}

grep_version() {
    grep -Ex '[[:digit:]]+[.][[:digit:]]+[.][[:digit:]]+([-][[:alnum:]]+)+'
}

# $1 : title
# $2 : path
info_file() {
    if [ -s "$2" ]; then
        info "$1:"
        while read -r line; do
            info "    ${line}"
        done <"$2"
    else
        info "$1: [EMPTY]"
    fi
}

info_eval() {
    if [ "true" = "${requested_noop:-false}" ]; then
        info "noop skipping:" "$@"
    else
        info "executing:" "$@"
        eval "$*"
    fi
}

################################################################################
################################################################################
################################################################################

while getopts ":hn" opt; do
    case "${opt}" in
        h)
            usage
            exit 0
            ;;
        n) readonly requested_noop="true" ;;
        :) die "missing argument: -${OPTARG}" ;;
        \?) die "bad option: -${OPTARG}" ;;
    esac
done
shift "$((OPTIND - 1))"

################################################################################

if ! version_current="$(uname -r | cut -d- -f1,2 | grep_version)"; then
    die "failed to get: version_current"
fi
readonly version_current="${version_current}"
info "version_current: ${version_current}"

if ! version_latest="$(
    find /boot -maxdepth 1 -type f -name 'vmlinuz-*' \
        | sort -V \
        | tail -1 \
        | cut -d- -f2,3 \
        | grep_version
)"; then
    die "failed to get: version_latest"
fi
readonly version_latest="${version_latest}"
info "version_latest: ${version_latest}"

if ! os_id="$(. /etc/os-release && echo "${ID}")"; then
    die "failed to get: os_id"
fi
readonly os_id="${os_id}"
case "${os_id}" in
    ubuntu) suffix="generic" ;;
    debian) suffix="$(dpkg --print-architecture)" ;;
    *) die "unsupported os_id:" "${os_id}" ;;
esac
info "os_id:" "${os_id}"

# don't purge packages needed by linux-image-generic or linux-headers-generic
if ! version_generic="$(
    apt-cache depends linux-image-"${suffix}" \
        | sed -En "s/^  Depends: linux-image-([[:digit:].-]+)-${suffix}$/\1/p" \
        | grep -Ex '[[:digit:]]+[.][[:digit:]]+[.][[:digit:]]+[-][[:digit:]]+'
)"; then
    die "failure: get_version_generic"
fi
info "version_generic: ${version_generic}"

# construct have_list
have_list="$(mktemp -t have_list.XXXXXX)"
readonly have_list="${have_list}"
(
    find /boot -maxdepth 1 -type f ! -name 'memtest*' ! -name 'initrd.img*'
    find /usr/src -maxdepth 1 -type d -name 'linux-*'
) >"${have_list}"
sort -Vuo "${have_list}" "${have_list}"
info_file 'have these' "${have_list}"

# construct keep_list
keep_list="$(mktemp -t keep_list.XXXXXX)"
readonly keep_list="${keep_list}"
grep -E "(${version_current}|${version_latest})" <"${have_list}" >"${keep_list}"
sort -Vuo "${keep_list}" "${keep_list}"
info_file 'keep these' "${keep_list}"

# construct kill_list
kill_list="$(mktemp -t kill_list.XXXXXX)"
readonly kill_list="${kill_list}"
# Do 'comm --nocheck-order' because 'comm' doesn't have the (GNU-extension)
# -V,--version-sort functionality that 'sort' does.
comm --nocheck-order -23 "${have_list}" "${keep_list}" >"${kill_list}"
info_file 'kill these' "${kill_list}"

# construct purge list
purge_list="$(mktemp -t purge_list.XXXXXX)"
readonly purge_list="${purge_list}"
xargs -r dpkg -S <"${kill_list}" \
    | cut -d: -f1 \
    | tr -s ',[:space:]' '\n' \
    | grep -Fv "${version_generic}" \
    | sort -u >"${purge_list}"
info_file 'purge these' "${purge_list}"

# purge the packages in the purge list
info_eval dpkg --configure -a
info_eval apt-get -y update
info_eval apt-get -yf purge "$(xargs -r <"${purge_list}")"
info_eval apt-get -y autoremove
info_eval apt-get -y autoclean
info_eval apt-get install -fy

# Upgrade all upgradables.
apt-cache show '?upgradable' \
    | grep-dctrl --no-field-names --show-field=Package --pattern="" \
    | sort -u \
    | xargs -r apt-get install --yes --upgrade

exit "$?"

# Upgrade all upgradables.
apt list --upgradable \
    | awk -F'/' '/upgradable/{print $1}' \
    | xargs apt-get install --yes --upgrade

exit "$?"
