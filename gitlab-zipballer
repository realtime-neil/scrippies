#!/bin/sh
# shellcheck disable=SC2317

this="$(readlink -f "$0")"
readonly this="${this}"
here="$(dirname "${this}")"
readonly here="${here}"
whatami="$(basename "${this}")"
readonly whatami="${whatami}"

log() { echo "${whatami}[$$]:" "$@" >&2; }
error() { log "ERROR:" "$@"; }
warning() { log "WARNING:" "$@"; }
info() { log "INFO:" "$@"; }

die() {
    error "$@"
    usage >&2
    exit 1
}

cleanup() {
    status="$?"
    return "${status}"
}

usage() {
    cat <<EOF
Usage: $0 [OPTION]...
[REQUIRED] "$GITLAB_TOKEN" env var must be defined as per documentation:
https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html

Scrape gitlab and download artifact zipball from the most recent pipeline 
jobs for a given project.

Options:

    -c    number of tarballs 
    -h    print usage and exit

Examples:

    \$ $0
    
    \$ export GITLAB_TOKEN=<personal_access_token>
    \$ $0 -p "realtime-robotics/rapidplan" -j "ci"

EOF
}

################################################################################
# https://docs.gitlab.com/ee/api/#keyset-based-pagination
# https://docs.gitlab.com/ee/api/pipelines.html
# https://docs.gitlab.com/ee/api/job_artifacts.html
################################################################################

trap cleanup EXIT

# CHECK USAGE
if [ -z "${GITLAB_TOKEN:+x}" ]; then
    die "undefined/empty GITLAB_TOKEN"
fi

RTR_GROUP_ID="4074339"
GITLAB_HOST_URL="https://gitlab.com/api/v4"
GITLAB_HEADER="PRIVATE-TOKEN: ${GITLAB_TOKEN}"
ARTIFACT_COUNT=16
GITLAB_JOB_NAME="ci"
GITLAB_PATH="realtime-robotics/rapidplan"

while getopts ":hc:j:p:" opt; do
    case "${opt}" in
        h)
            usage
            exit 0
            ;;
        c) ARTIFACT_COUNT="${OPTARG}" ;;
        j) GITLAB_JOB_NAME="${OPTARG}" ;;
        p) GITLAB_PATH="${OPTARG}" ;;
        :) die "missing argument: -${OPTARG}" ;;
        \?) die "bad option: -${OPTARG}" ;;
    esac
done

info "GITLAB_PATH: ${GITLAB_PATH}"
info "GITLAB_JOB_NAME: ${GITLAB_JOB_NAME}"
info "ARTIFACT_COUNT: ${ARTIFACT_COUNT}"

set -euvx

# retrieve all project ids and find the one that matched with our path name
# RAPIDPLAN_PROJECT_ID="27169571"
PROJECT_ID=$(curl --retry 3 --header "${GITLAB_HEADER}" "${GITLAB_HOST_URL}/groups/$RTR_GROUP_ID" | tr '\r\n' ' ' \
    | jq --arg PATH "${GITLAB_PATH}" '. | .projects[] | select(.path_with_namespace==$PATH) | .id')
GITLAB_PROJECT_URL="${GITLAB_HOST_URL}/projects/${PROJECT_ID}"

if [ -z "${PROJECT_ID-}" ]; then
    error "GITLAB_PATH: [${GITLAB_PATH}] is not valid or does not exist"
    exit 2
fi

PROJECT_PIPELINES=$(curl --retry 3 --header "${GITLAB_HEADER}" "${GITLAB_PROJECT_URL}/pipelines?status=success&per_page=${ARTIFACT_COUNT}" \
    | tr '\r\n' ' ' | jq '.[] | .id')

if [ -z "${PROJECT_PIPELINES-}" ]; then
    error "PROJECT_PIPELINES: [${PROJECT_PIPELINES}] does not exist on project ID [${PROJECT_ID}]"
    exit 2
fi

echo "${PROJECT_PIPELINES}" | while read -r pipeline_id; do
    PIPELINES_JOBS=$(curl --retry 3 --header "${GITLAB_HEADER}" "${GITLAB_PROJECT_URL}/pipelines/${pipeline_id}/jobs?scope[]=success" \
        | tr '\r\n' ' ' | jq --arg NAME "${GITLAB_JOB_NAME}" '.[] | select(.name==$NAME) | .id')

    echo "${PIPELINES_JOBS}" | while read -r job_id; do
        curl --location --output "${job_id}.zip" --header "${GITLAB_HEADER}" "${GITLAB_PROJECT_URL}/jobs/${job_id}/artifacts"
    done
done
