#!/bin/sh
# shellcheck disable=SC2317
# scrippies/configure-apt

set -eu

export LC_ALL=C

this="$(realpath "$0")"
readonly this="${this}"
here="$(dirname "${this}")"
readonly here="${here}"
whatami="$(basename "${this}")"
readonly whatami="${whatami}"
tmpdir="$(
    mktemp -dt "${whatami}.XXXXXX" \
        | xargs -I{} sh -c "chmod 1777 {} && echo {}"
)"
readonly tmpdir="${tmpdir}"
export TMPDIR="${tmpdir}"

cleanup() {
    status="$?"
    rm -rf "${tmpdir}" || true
    return "${status}"
}
trap cleanup EXIT

log() { echo "${whatami}[$$]:" "$@" >&2; }
error() { log "ERROR:" "$@"; }
warning() { log "WARNING:" "$@"; }
info() { log "INFO:" "$@"; }
die() {
    error "$@"
    usage >&2
    exit 1
}

usage() {
    cat <<EOF

Usage: $0 [OPTION]...
Configure apt to use additional repositories.

Options:

    -h                 print usage and exit
    -D DISTRIBUTION    override DISTRIBUTION (default: detected)

Examples:

    \$ $0 -h

    \$ $0

    \$ $0 -D focal

EOF
}

################################################################################
################################################################################
################################################################################

while getopts ":hD:" opt; do
    case "${opt}" in
        h)
            usage
            exit 0
            ;;
        D) readonly distribution="${OPTARG}" ;;
        :) die "missing argument: -${OPTARG}" ;;
        \?) die "bad option: -${OPTARG}" ;;
    esac
done
shift "$((OPTIND - 1))"

if [ -n "${distribution-}" ]; then
    info "distribution: ${distribution}"
elif distribution="$(. /etc/os-release && echo "${VERSION_CODENAME}")"; then
    readonly distribution="${distribution}"
    warning "detected: distribution: ${distribution}"
else
    die "undefined/undetected DISTRIBUTION"
fi

################################################################################

eval "$(apt-config shell DIR_ETC_PARTS Dir::Etc::parts/f)"
eval "$(apt-config shell DIR_ETC_SOURCEPARTS Dir::Etc::sourceparts/f)"
eval "$(apt-config shell DIR_ETC_TRUSTEDPARTS Dir::Etc::trustedparts/f)"
export DIR_ETC_PARTS="${DIR_ETC_PARTS}"
export DIR_ETC_SOURCEPARTS="${DIR_ETC_SOURCEPARTS}"
export DIR_ETC_TRUSTEDPARTS="${DIR_ETC_TRUSTEDPARTS}"
info "DIR_ETC_PARTS:" "${DIR_ETC_PARTS}"
info "DIR_ETC_SOURCEPARTS:" "${DIR_ETC_SOURCEPARTS}"
info "DIR_ETC_TRUSTEDPARTS:" "${DIR_ETC_TRUSTEDPARTS}"

################################################################################

############################################
# PROACTIVE PROTECTION AGAINST A BAD PROXY #
############################################
#
# * https://askubuntu.com/questions/679233/failed-to-fetch-hash-sum-mismatch-tried-rm-apt-list-but-didnt-work
# * https://serverfault.com/questions/722893/debian-mirror-hash-sum-mismatch
# * https://gist.github.com/trastle/5722089
cat >"${DIR_ETC_PARTS}/99-fixbadproxy.conf" <<'EOF'
Acquire::http::Pipeline-Depth "0";
Acquire::http::No-Cache=True;
Acquire::BrokenProxy=true;
EOF

################################################################################

###################################################
# bootstrap ppa:realtime-robotics/ubuntu/keyrings #
###################################################

# install DEB822-style sources.list file to
# /etc/apt/sources.list.d/*.sources
cat >"${DIR_ETC_SOURCEPARTS}/keyrings.sources" <<EOF
Types: deb deb-src
URIs: http://ppa.launchpad.net/realtime-robotics/keyrings/ubuntu
Suites: ${distribution}
Components: main
EOF

# install gnupg public key file to /etc/apt/trusted.gpg.d/*.asc
#
# $ gpg -k 0xae0f78ce556b80ab48ce556f208dac2d8a68278a
# pub   rsa4096 2020-03-16 [SC]
#       AE0F78CE556B80AB48CE556F208DAC2D8A68278A
# uid           [ unknown] Launchpad PPA for realtime-robotics
#
# $ gpg --armor --export --export-options=export-minimal 0xae0f78ce556b80ab48ce556f208dac2d8a68278a
cat >"${DIR_ETC_TRUSTEDPARTS}/keyrings.asc" <<'EOF'
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQINBF5vsKcBEACw/lMOWu4aQBG2155Tqji9x7S7QqOIoXdzHLu1J7uih3DHweoS
WniSjaL3HZGTjyte/h2JZLRsEeEOLeCd8UfYatAmZowlMfiF5NkbZx2bvLrepGfc
/IY9stEA8lCihlowaWSmbZQII4P12SFUJTpSOoScc22NVebgZ2f/X85NgyDOBxyn
j1QA7N1LMVJntWFLYz6ARlkEtLwyzX+v7Z/5SU9H5frRW0WLs8qpRwvtS3ysNOO+
r4/vUM79okQCj7pm4tUMNHG2at5fZ5OilyYzGb9+3aRXVpuij9jDUq04UHSVeazf
UHMOgOVtOp/sWfBD0GNRclsqdvkV3tPc5j+nL+RlXD2Ou6SOT5RgosmTB/4J53SX
30A110r3tP4Ng1HCzLweyX8qUxZ2jGp0iy/p2WYqNHXw0Rjx3aUpNU+23zSQCmi9
um4/Vr1KsUgOHKgK9hi1xBGYOO5g8XvKwZlJA5t7c+3lLNxm7fKO4rxYbDcZqbXR
sw/8EDcQUAFzxCFdCwoTrll7E7afQtOqGSlrh3XtfCXegsiZ3t9vjXaZ7YeC4S09
fcLUnQxuJcY6PKxFfomPzbzwIhrYIwvOcBw92RVOFA8D+IWBQ1KS8cRvultFwDUW
+P8R4blkStZVdjQ2g5G2y/VpMvoaflrwGxo3Yingc+nD4qysOCxQkyYFlwARAQAB
tCNMYXVuY2hwYWQgUFBBIGZvciByZWFsdGltZS1yb2JvdGljc4kCOAQTAQIAIgUC
Xm+wpwIbAwYLCQgHAwIGFQgCCQoLBBYCAwECHgECF4AACgkQII2sLYpoJ4oe1BAA
ktoNpKxfriOJzhwAFTsF2XFJ0rM6WjYtOVAoIxJXG5c8jOcY5WtZ14/hx4DrK3nf
z/WMDN8xaCzBOeLZ3V12IUKJ2LHZSmkxqfcd2mlzrJ3kYDnl7q6UEjCwk8ly4JG1
5L5A8E5rnEU/Q26jy+X3DosKFuTPk61H0xOfBdkeQOckXcpphKi468d9qXhoQcSB
JgDxuFLU34JKJIj8HmtWGTjGtQJ/66nyf0FBWaB0r6Q+Si5mr26dy7TEWmu6OzXO
rtrxdM0kiI1NPCiq/l9kuTzgtr2CK/VxW17Vc6tGAFOvw7up75ZbbAnqnMSNrqdO
cmM2ZlSmT5CIzwwYuVWYdCX+m2GSiqOjZq9Ob1AKKxp/SEyoClR4tqKJmomd8kSG
rJneB0GeDjWTIZ5tMhgFdGpC/uDOcmxR3u6RytBV4QDp06Ds6mdLyLQavyI9xTRT
k7huI3vBzihDU03NdIwP7B5XpLLuvsNGFCcWbMf5jXa0LUsz87K62isf/k+kj+s7
d7KoqI4jNU1ezdBDF9RLgH/95vtxPxHPwXDOo6+tBhtfCFSdunEJI5EVrJwvj/B5
ozM0yFMCDZQqpZ/CAxH9CjxIPM21vennMKuwikGOkDj2pjKmHpWGWwIRuGUi30cc
+Lsb1DP4pkVCeaMupLVMxPzStktUUea6A23Exu8WNq8=
=T7TV
-----END PGP PUBLIC KEY BLOCK-----
EOF

# install everything published in ppa:realtime-robotics/ubuntu/keyrings
apt-get --yes update
apt-get --yes install rtr-archive-all

# bootstrap is done; nuke the bootstraps
rm -v "${DIR_ETC_SOURCEPARTS}/keyrings.sources"
rm -v "${DIR_ETC_TRUSTEDPARTS}/keyrings.asc"

################################################################################

######################################################
# PACKAGECLOUD.IO REALTIME-ROBOTICS RTR-ROBOT-MODELS #
######################################################

if ! [ -x "${here}/packagecloud-apt" ]; then
    die "missing executable: ${here}/packagecloud-apt"
fi
(
    export PACKAGECLOUD_READ_TOKEN="${RTR_ROBOT_MODELS_READ_TOKEN-}"
    export PACKAGECLOUD_MASTER_TOKEN="${RTR_ROBOT_MODELS_MASTER_TOKEN-}"
    eval "${here}/packagecloud-apt" \
        --account "realtime-robotics" \
        --repo "rtr-robot-models"
)

################################################################################

exit "$?"
